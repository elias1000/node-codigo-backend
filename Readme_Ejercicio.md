
Dependencias:

dotenv: Es un modulo para la carga de varibles de entorno desde un archivo .env , de esta manera tenemos el valor de un dato sensible oculto y solo se muestra el nombre de la variable.

express: es un entorno de trabajo para realizar aplicaciones web, soporte los distintos verbos HTTP, se puede renderizar vistas, configurar puertos, agregarle mas funcionalidades con paquetes middleware y bibliotecas.

serve: se utiliza para indexar el contenido del directorio, en el curso todavia no lo utilizamos.

ts-node: sirve para transformar typescript en codigo javascript, por ejemplo el codigo escrito en el index.ts se transforma en el archivo index.js.


Dependencias dev:

concurrently: lo usamos para ejecutar varios comandos al mismo tiempo en la terminal,podemos darle diferentes salidas al mismo tiempo en un solo comando.

eslint: es una libreria que sirve para analizar el codigo y detectar patrones que no sean correctos en cuanto al codigo js.

eslint-config-standard-with-typescript: agrega las reglas de typescript a la libreria eslint.

eslint-plugin-import: agrega reglas/Deteccion en cuanto a problemas de importacion de codigo es2015 y es6.

eslint-plugin-n: agrega la deteccion de problemas en cuanto al codigo node.

eslint-plugin-promise: detecta patrones en cuanto a las promesas en javascript.

jest: es un framework de codigo abierto para realizar test de codigo javascript.

nodemon: es una herramienta que usamos para reiniciar automaticamente node cuando hagamos un cambio, sin necesidad de que manualmente arranquemos el servidor.

supertest: es una herramienta para probar las operaciones HTTP

ts-jest: herramienta para probar proyectos escritos en typescript.

typescript: es un lenguaje para JS que permite agregar tipos opcionales a javascript, se compila en JS basado en estandares.

webpack: permite agrupar los archivos de JS para utilizarlos en el navegador.

webpack-cli: proporciona comandos para configurar el proyecto web/webpack personalizado.

webpack-node-externals: permite definir elementos externos que no se deben agrupar, como el node_modules.

webpack-shell-plugin: permite ejecutar comandos shell antes o despues de compilar el paquete web.

--------Scripts de NPM-----

"build": "npx tsc": sirve para ejecutar el compilador de typescript
"start": "node dist/index.js": ejecuta el index.js que se encuentra en la carpeta dist.
"dev": "concurrently \"npx tsc --watch\" \"nodemon -q dist/index.js\"": se observan todos los cambios y se compilan con tsc watch y se ejecuta nodemon para el archivo index.js.
"test": "jest": se ejecuta jest para correr el suite de pruebas.
"serve:coverage": " npm run test && cd coverage/lcov-report && npx serve": se ejecutan los test y se almacenan en la carpeta coverage.

------variables de entorno.

al momento del curso solo se genero la variable PORT con valor 8000.