import server from './src/server';
import { LogError, LogSuccess } from './src/utils/logger';

//enviroment variables
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config();

const port = process.env.PORT || 8000;


// * execute server
server.listen(port, ()=>{
    LogSuccess(`[SERVER ON]: Running in http://localhost:${port}/api`);

});

// * Control server error
server.on('error', (error) =>{
    LogError(`[SERVER ERROR]: ${error}`);
});


