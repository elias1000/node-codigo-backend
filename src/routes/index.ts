
/**
 * Root Router
 * Redirections to routers
 */


import express, { Request, Response } from "express";
import helloRouter from './HelloRouter';
import { LogInfo } from "../utils/logger";


// server instance
let server = express();


//router instance

let rootRouter = express.Router();


// activate for request to http://localhost:8000/api


//get http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response)=> {
    LogInfo('GET: http://localhost:8000/api/')
    //send hello world
    res.send('Welcome to API Restul Express + Nodemon + Jest + TS + Swagger + Mongoose');

});


//redirections to routers & controllers
server.use('/', rootRouter); // http://localhost:8000/api/
server.use('/hello', helloRouter); //  http://localhost:8000/api/hello --> hello router
// add more router to the app

export default server;
