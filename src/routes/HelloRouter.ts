import express, { Request, Response } from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";
import { convertCompilerOptionsFromJson } from "typescript";


//Router from express 

let helloRouter = express.Router();

//GET -> //http//localhost:8000/api/hello?name=Elias/
helloRouter.route('/')
    //GET
    .get(async(req: Request, res: Response)=>{
        //obtener query param
        let name: any = req?.query?.name;
        LogInfo(`Query param: ${name}`);
        // instancia de controller  para ejecutar el metodo
        const controller: HelloController = new HelloController();
        //obtener la respuesta
        const response = await controller.getMessage(name);

        //enviar al cliente la respuesta
        return res.send(response);
    })


    //export hello router

    export default helloRouter;