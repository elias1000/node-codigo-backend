import express, { Express, Request, Response } from "express";




// security
import cors from 'cors';
import helmet from 'helmet';


//TODO HTTPS

//root router
import rootRuter from '../routes';





//create express app
const server: Express= express();


//define server to use "/api" and use rootRouter from 'index.ts' in routes
// from this point onover: http://localhost:8000/api/...
server.use(
    '/api',
    rootRuter
    );

//TODO: Monggose connection


// * Security config
server.use(helmet());
server.use(cors());

// * Content Type Config
server.use(express.urlencoded({extended: true, limit: '50mb'}));
server.use(express.json({limit: '50mb'}));

// * Redirection config
//http://localhost:8000/ --> redirigir a /api
server.get('/',(req:Request, res: Response)=>{
    res.redirect('/api');
})

export default server;
